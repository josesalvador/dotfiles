===
Vim
===



:Autores: José A. Salvador Vanaclocha, Numenalia (Numenalia, S.L.) september 2014
:Versión: 1.8 diciembre 2018



.. raw:: pdf

    PageBreak


.. contents:: Indice


.. section-numbering::


.. raw:: pdf

    PageBreak 



Synopsis
========

Some useful information about vim.


YouCompleteMe
=============

YouCompleteMe is a vim plugin which offers autocompletion for a various kind of languages.

To install and setup YouCompleteMe follow instructions at [https://github.com/Valloric/YouCompleteMe] but if you can not wait a minute here we go with a basic instruction set to get ready:

+ Make sure you have this packages installed:
  - build-essential cmake python-dev
+ Install YouCompleteMe as a Plugin with Vundle
+ Compìle YouCompleteMe (in my case with c-lang and c# support)


.. code-block:: console

    $ cd ~/.vim/bundle/YouCompleteMe
    $ ./install.sh --clang-completer --omnisharp-completer

