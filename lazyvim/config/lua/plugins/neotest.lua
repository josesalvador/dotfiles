return {
  {
    "nvim-neotest/neotest",
    dependencies = {
      "nvim-neotest/neotest-python",
      "marilari88/neotest-vitest",
    },
    opts = {
      adapters = {
        ["neotest-python"] = {
          runner = "django",
          -- TODO jasv 20240314 What I have to do to execute Django test with no -input option in Nvim instead of execute it in the shell
          --  clear && ./manage.py test siex.tests.infrastructure.repositories.test_rea_farm --settings=agroptima.settings.test --noinput --parallel
          args = { "--keepdb" },
        },
        "neotest-vitest",
      },
      output = { open_on_run = false },
    },
  },
}
