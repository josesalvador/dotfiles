return {
  {
    "folke/tokyonight.nvim",
    opts = {
      style = "night",
      -- style = "moon",
      -- [theme moon colors](https://github.com/folke/tokyonight.nvim/blob/main/extras/lua/tokyonight_moon.lua)
      -- on_colors = function(colors)
      --   colors.bg_popup = "#ffffff"
      --   colors.bg_sidebar = "#ffffff"
      -- end,
    },
  },
}
