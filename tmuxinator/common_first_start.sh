#!/bin/sh

set -e

CODENAME=$(lsb_release -c | awk '{print $2}')
cd ~/git/dotfiles && git fetch --all --tags --prune && git checkout $CODENAME && git merge --ff-only origin/$CODENAME && cd -
pyenv global system && ansible-playbook -c local --ask-become-pass ~/git/dotfiles/ansible/site.yml
thunderbird >/dev/null 2>&1 &
firefox >/dev/null 2>&1 &
telegram-desktop >/dev/null 2>&1 &
