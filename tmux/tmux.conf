set -g prefix C-a
unbind C-b
bind C-a send-prefix

# move around panes with hjkl, as one would in vim after pressing ctrl-w
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# resize panes like vim
# feel free to change the "1" to however many lines you want to resize by, only
# one at a time can be slow
bind < resize-pane -L 10 
bind > resize-pane -R 10
bind - resize-pane -D 10
bind + resize-pane -U 10 

# new windows and panes with the current working directory
bind-key c new-window -c "#{pane_current_path}"
bind-key % split-window -h -c "#{pane_current_path}"
bind-key '"' split-window -v -c "#{pane_current_path}"

# Reduce escape time
set-option -sg escape-time 10

# Enable focus events
set-option -g focus-events on

# Set RGB capabilities
set-option -g default-terminal "screen-256color"
set-option -sa terminal-overrides ',xterm-256color:RGB'

# Reload tmux
bind-key r source-file ~/.tmux.conf \; display-message "Config reloaded!"
