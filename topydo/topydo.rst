======
Topydo
======



:Author: José A. Salvador Vanaclocha, Numenalia (Numenalia, S.L.) november 2017
:Version: 1.2 december 2018



.. raw:: pdf

    PageBreak


.. contents:: Index


.. section-numbering::


.. raw:: pdf

    PageBreak 



Info
====

You can obtain more info about topydo_, an application to manage tasks based in todotxt_ format.

.. _topydo: https://topydo.org

.. _todotxt: http://todotxt.org

