#!/bin/bash

# TODO jasv 20201008 enable when available
# sudo apt install software-properties-common
# sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible

ansible-playbook -c local --ask-become-pass ansible/site.yml

echo "To do"
echo "====="
echo ""
echo "- Set zsh as default shell: $ chsh -s $(which zsh)"
echo "- Sign in to Firefox"
echo "- You must reboot your session to complete bootstrapping process"
echo ""
echo ""
echo "Remember"
echo "========"
echo ""
echo "May be you want to get this computer into backup system"
echo ""
echo ""
echo "Recommended"
echo "==========="
echo ""
echo "- Launch and setup syncthing in order to use topydo and vimwiki. Don't forget to put syncthing to start on boot"
echo "- Hand made install and setup of Cardbook (Thunderbird plugin)"
echo "- Set shortcut Ctrl-Alt-F for Flameshot (command 'flameshot gui') to gnome settings"
echo ""
echo ""
echo ""
echo "That's all. Press enter to continue"
read
