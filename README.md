# Bootstrap a new host

```shell
$ ./bootstrap.sh
```


# Shell and vim colors

My dotfiles auto installs through *zshrc* file base16 shell script to get right 256 colorspace.

Vim install a base 16 plug too and sets in its vimrc the necessary code to use the base16 shell script color selected theme.

More info on:

- https://github.com/chriskempson/base16-shell
- https://github.com/chriskempson/base16-vim

In order to get working well the color scheme in tmux inside guake, tmux must be launched with *tmux -2* for Ubuntu < 18.04.X
